def nibble_to_ascii(nibble: int) -> str:
  """
  This is a comment
  Input: Nibble (4-bits)
  Output: Single character HEX as a string
  Example: Input = 10, Output = 'A'
  Example: Input = 8,  Output = '8'
  """
  table = ['0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F']
  return table[nibble]


def to_binary(number: int) -> str:
    """
    Input: Number (integer)
    Output: String
    Example: Input = 43605, Output = "0b1010101001010101"
    """
    answer = ""
    
    while True:
        quotient = number // 2
        remainder = number % 2
        answer = nibble_to_ascii(remainder) + answer
        number = quotient
        if (quotient == 0):
            break
    
    return "0b" + answer


print(to_binary(43605))
print(to_binary(123456789))
print(to_binary(0b1010101))
print(to_binary(0xDEADBEEF))